# Тестовое задание НК
## docker
### docker environment
1. Настроить в файле .env переменные UID и GID

Для получения UID
```host$ id -u ```

Для получения GID
```host$ id -g ```.
2. Настроить в файле .env подключение к базе данных
3. Запуск docker окружение ```host$ docker compose up -d```

### Задание параметров
Создать файл /app/configs/parameters.php
```docker-env$ mv ./configs/parameters.php.example ./configs/parameters.php```
и заполнить соответствующе поля.
### Миграции
#### Конфигурация базы данных
Создать файл /app/configs/db.php
```docker-env$ mv ./configs/db.php.example ./configs/db.php```
и заполнить доступы к базе данных.
#### Запуск миграций
```docker-env$ php bin/doctrine-migrations migrate```
