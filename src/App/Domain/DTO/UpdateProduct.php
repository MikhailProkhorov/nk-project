<?php

namespace App\Domain\DTO;

class UpdateProduct
{
    public function __construct(
        public int $id,
        public string $name,
        public int $inn,
        public int $barcode,
        public string $description,
        public array $categories = [],
    )
    {}
}
