<?php

namespace App\Domain\DTO;

class DeletedProduct
{
    public function __construct(
        public int $id,
        public string $name,
        public string $message = 'Was deleted'
    )
    {}
}
