<?php

namespace App\Domain\DTO;

class CreateProduct
{
    public function __construct(
        public string $name,
        public int $inn,
        public int $barcode,
        public string $description,
        public array $categories = [],
    )
    {}
}
