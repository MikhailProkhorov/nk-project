<?php

namespace App\Domain\Models;

use App\Domain\Collections\CategoryCollection;
use App\Domain\Contracts\ModelInterface;
use JsonSerializable;

class Product implements ModelInterface, JsonSerializable
{
    public function __construct(
        public ?int $id,
        public string $name,
        public int $inn,
        public int $barcode,
        public string $description,
        public ?CategoryCollection $categories = null,
    )
    {}

    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'inn' => $this->inn,
            'barcode' => $this->barcode,
            'description' => $this->description,
            'categories' => $this->categories,
        ];
    }
}
