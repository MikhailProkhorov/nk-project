<?php

namespace App\Domain\Models;

use App\Domain\Contracts\ModelInterface;
use JsonSerializable;

class Category implements ModelInterface, JsonSerializable
{
    public function __construct(
        public int $id,
        public string $name
    )
    {}

    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
        ];
    }
}
