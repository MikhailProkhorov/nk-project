<?php

namespace App\Domain\Shared\Collections;

use App\Domain\Shared\Models\Error;
use ArrayObject;
use IteratorAggregate;

class ErrorCollection implements IteratorAggregate, \Countable
{
    /**
     * @var Error[]|array
     */
    private $errors = [];

    /**
     * @param Error $error
     * @return void
     */
    public function add(Error $error): void
    {
        $this->errors[] = $error;
    }

    /**
     * @return ArrayObject
     */
    public function getIterator(): ArrayObject
    {
        return new ArrayObject($this->errors);
    }

    public function count(): int
    {
        return count($this->errors);
    }
}
