<?php

namespace App\Domain\Shared\Models;

class Error
{
    public function __construct(
        public string $message,
    )
    {}

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }
}
