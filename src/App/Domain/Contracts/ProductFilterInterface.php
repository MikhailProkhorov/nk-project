<?php

namespace App\Domain\Contracts;

interface ProductsFilterInterface
{
    public function byName(): array;

    public function byBarcode(): array;

    public function byInn(): array;

    public function byCategory(): array;
}
