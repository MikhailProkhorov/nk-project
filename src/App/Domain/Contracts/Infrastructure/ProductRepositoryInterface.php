<?php

namespace App\Domain\Contracts\Infrastructure;

use App\Domain\Collections\ProductCollection;
use App\Domain\DTO\CreateProduct;
use App\Domain\DTO\DeletedProduct;
use App\Domain\DTO\UpdateProduct;
use App\Domain\Models\Product;

interface ProductRepositoryInterface extends ModelRepositoryInterface
{
    public function getById(int $id): ?Product;

    public function create(CreateProduct $createProduct): Product;

    public function isUnique(int $inn, int $barcode): bool;

    public function findProductsAll(string $searchQuery = ''): ProductCollection;

    public function delete(int $productId): void;

    public function findBySearchQuery(string $searchQuery): ?ProductCollection;

    public function update(UpdateProduct $updateProduct): Product;

    public static function modelFromEntity(mixed $entity): Product;

    public static function modelsFromEntityCollection(array $entities): ProductCollection;
}

