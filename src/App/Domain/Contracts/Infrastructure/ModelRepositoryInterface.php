<?php

namespace App\Domain\Contracts\Infrastructure;

interface ModelRepositoryInterface
{
    /**
     * @param mixed $entity
     * @return mixed
     */
    public static function modelFromEntity(mixed $entity): mixed;

    /**
     * @param array $entities
     * @return mixed
     */
    public static function modelsFromEntityCollection(array $entities): mixed;
}
