<?php

namespace App\Domain\Contracts\Infrastructure;

use App\Domain\Collections\CategoryCollection;
use App\Domain\Models\Category;

interface CategoryRepositoryInterface extends ModelRepositoryInterface
{
    public function findListById(array $idList): CategoryCollection;

    /**
     * @param mixed $entity
     * @return Category
     */
    public static function modelFromEntity(mixed $entity): Category;

    /**
     * @param array $entities
     * @return CategoryCollection
     */
    public static function modelsFromEntityCollection(array $entities): CategoryCollection;
}
