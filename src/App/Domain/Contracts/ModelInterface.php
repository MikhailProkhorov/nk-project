<?php

namespace App\Domain\Contracts;

interface ModelInterface
{
    public function getId(): int;
}
