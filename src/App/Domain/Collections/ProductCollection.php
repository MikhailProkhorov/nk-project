<?php

namespace App\Domain\Collections;

use App\Domain\Models\Product;
use ArrayObject;
use IteratorAggregate;
use JsonSerializable;

class ProductCollection implements IteratorAggregate, JsonSerializable
{
    /**
     * @var Product[]|array
     */
    private array $items = [];

    /**
     * @param Product[]|array $items
     */
    public function __construct(array $items)
    {
        foreach ($items as $item) {
            $this->add($item);
        }
    }

    /**
     * @param Product $item
     * @return void
     */
    public function add(Product $item): void
    {
        $this->items[$item->id] = $item;
    }

    /**
     * @return ArrayObject
     */
    public function getIterator(): ArrayObject
    {
        return new ArrayObject($this->items);
    }

    public function jsonSerialize(): mixed
    {
        return array_map(fn($item) => $item->jsonSerialize(), $this->items);
    }
}
