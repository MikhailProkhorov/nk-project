<?php

namespace App\Domain\Collections;

use App\Domain\Models\Category;
use ArrayObject;
use IteratorAggregate;
use JsonSerializable;

class CategoryCollection implements IteratorAggregate, JsonSerializable
{
    /**
     * @var Category[]|array
     */
    private array $items = [];
    /**
     * @param Category[]|array $items
     */
    public function __construct(array $items)
    {
        foreach ($items as $item) {
            $this->add($item);
        }
    }

    /**
     * @param Category $item
     * @return void
     */
    public function add(Category $item): void
    {
        $this->items[$item->id] = $item;
    }

    /**
     * @return ArrayObject
     */
    public function getIterator(): ArrayObject
    {
        return new ArrayObject($this->items);
    }

    public function jsonSerialize(): mixed
    {
        return array_map(static fn($item) => $item->jsonSerialize(), $this->items);
    }
}
