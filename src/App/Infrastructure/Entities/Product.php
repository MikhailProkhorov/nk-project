<?php

namespace App\Infrastructure\Entities;

use App\Infrastructure\Repositories\ProductRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\PrePersist;
use Doctrine\ORM\Mapping\PreUpdate;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\UniqueConstraint;

#[Entity(repositoryClass: ProductRepository::class)]
#[Table(name: 'products')]
#[UniqueConstraint(name: 'inn_barcode', columns: ['inn', 'barcode'])]
#[HasLifecycleCallbacks]
class Product
{
    public function __construct()
    {
        $this->categories = new ArrayCollection();
    }

    #[Id]
    #[Column(type: 'integer')]
    #[GeneratedValue]
    private int $id;

    #[Column(type: 'string', nullable: false)]
    private string $name;

    #[Column(type: 'string', columnDefinition: 'CHAR(10) NOT NULL')]
    private int $inn;

    #[Column(type: 'string', columnDefinition: 'CHAR(13) NOT NULL')]
    private int $barcode;

    #[Column(type: 'string', nullable: false)]
    private string $description;

    /**
     * @var Collection<int, Category>
     */
    #[ManyToMany(targetEntity: Category::class, inversedBy: 'products')]
    #[JoinTable(name: 'products_categories')]
    private Collection $categories;

    #[PrePersist, PreUpdate]
    public function assertIsInnWithBarcodeUnique()
    {}

    /**
     * @return Collection
     */
    public function getCategories(): Collection
    {
        return $this->categories;
    }

    /**
     * @param Collection $categories
     * @return Product
     */
    public function setCategories(Collection $categories): Product
    {
        $this->categories = $categories;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Product
     */
    public function setName(string $name): Product
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return int
     */
    public function getInn(): int
    {
        return $this->inn;
    }

    /**
     * @param int $inn
     * @return Product
     */
    public function setInn(int $inn): Product
    {
        $this->inn = $inn;
        return $this;
    }

    /**
     * @return int
     */
    public function getBarcode(): int
    {
        return $this->barcode;
    }

    /**
     * @param int $barcode
     * @return Product
     */
    public function setBarcode(int $barcode): Product
    {
        $this->barcode = $barcode;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Product
     */
    public function setDescription(string $description): Product
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
}
