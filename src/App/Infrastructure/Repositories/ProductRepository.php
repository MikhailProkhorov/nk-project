<?php

namespace App\Infrastructure\Repositories;

use App\Domain\Collections\ProductCollection;
use App\Domain\Contracts\Infrastructure\ProductRepositoryInterface;
use App\Domain\DTO\CreateProduct;
use App\Domain\DTO\UpdateProduct;
use App\Domain\Models\Product;
use App\Infrastructure\Entities\Product as ProductEntity;
use Doctrine\ORM\EntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Query\Expr;

class ProductRepository extends EntityRepository implements ProductRepositoryInterface
{
    /**
     * @param int $id
     * @return Product|null
     */
    public function getById(int $id): ?Product
    {
        /** @var ProductEntity $product */
        $product = $this->findOneBy(['id' => $id]);
        if (!$product) {
            return null;
        }

        return static::modelFromEntity($product);
    }

    public function create(CreateProduct $createProduct): Product
    {
        $product = new ProductEntity();
        $product->setName($createProduct->name)
            ->setInn($createProduct->inn)
            ->setBarcode($createProduct->barcode)
            ->setDescription($createProduct->description)
            ->setCategories(new ArrayCollection($createProduct->categories));

        $this->getEntityManager()->persist($product);
        $this->getEntityManager()->flush();
        return static::modelFromEntity($product);
    }

    public function isUnique(int $inn, int $barcode): bool
    {
        return $this->count(['inn' => $inn, 'barcode' => $barcode]) === 0;
    }

    /**
     * @param ProductEntity $entity
     * @return Product
     */
    public static function modelFromEntity($entity): Product
    {
        return new Product(
            id: $entity->getId(),
            name: $entity->getName(),
            inn: $entity->getInn(),
            barcode: $entity->getBarcode(),
            description: $entity->getDescription(),
            categories: CategoryRepository::modelsFromEntityCollection(
                $entity->getCategories()->toArray()
            )
        );
    }

    /**
     * @param array $entities
     * @return ProductCollection
     */
    public static function modelsFromEntityCollection(array $entities): ProductCollection
    {
        return new ProductCollection(
            array_map(
                static fn($entity) => static::modelFromEntity($entity),
                $entities
            )
        );
    }

    public function findProductsAll(string $searchQuery = ''): ProductCollection
    {
        return static::modelsFromEntityCollection($this->findAll());
    }

    public function delete(int $productId): void
    {
        $this->getEntityManager()->remove($this->findOneBy(['id' => $productId]));
        $this->getEntityManager()->flush();
    }

    public function findBySearchQuery(string $searchQuery): ?ProductCollection
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();
//        $query = $queryBuilder->select(['p', 'c'])
        $query = $queryBuilder->select(['p', 'c'])
            ->from(ProductEntity::class, 'p')
            ->leftJoin(
                'p.categories',
                'c',
                'WITH'
            )
            ->where($queryBuilder->expr()->orX(
                $queryBuilder->expr()->like('LOWER(p.name)', ':search'),
                $queryBuilder->expr()->like('LOWER(p.barcode)', ':search'),
                $queryBuilder->expr()->like('LOWER(p.inn)', ':search'),
                $queryBuilder->expr()->like('LOWER(c.name)', ':search'),
            ))->setParameter('search', '%' . $searchQuery . '%');
        return static::modelsFromEntityCollection($query->getQuery()->getResult());
    }

    public function update(UpdateProduct $updateProduct): Product
    {
        $product = $this->findOneBy(['id' => $updateProduct->id]);
        $product->setName($updateProduct->name)
            ->setInn($updateProduct->inn)
            ->setBarcode($updateProduct->barcode)
            ->setDescription($updateProduct->description);

        $this->getEntityManager()->flush();

        return static::modelFromEntity($product);
    }
}
