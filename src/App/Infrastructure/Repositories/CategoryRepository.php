<?php

namespace App\Infrastructure\Repositories;

use App\Domain\Collections\CategoryCollection;
use App\Domain\Contracts\Infrastructure\CategoryRepositoryInterface;
use App\Domain\Models\Category;
use App\Infrastructure\Entities\Category as CategoryEntity;
use Doctrine\ORM\EntityRepository;
use Doctrine\Common\Collections\Collection;

class CategoryRepository extends EntityRepository implements CategoryRepositoryInterface
{
    /**
     * @param CategoryEntity[]|array $idList
     * @return array
     */
    public function findListById(array $idList): CategoryCollection
    {
        return self::modelsFromEntityCollection($this->findBy(['id' => $idList]));
    }

    public static function modelFromEntity(mixed $entity): Category
    {
        return new Category(
            id: $entity->getId(),
            name: $entity->getName(),
        );
    }

    public static function modelsFromEntityCollection(array $entities): CategoryCollection
    {
        return new CategoryCollection(array_map(
            static fn ($entity) => static::modelFromEntity($entity),
            $entities
        ));
    }
}
