<?php

namespace App\Infrastructure;

use App\Application\Contracts\DaDataClientInterface;

class DummyDataClient implements DaDataClientInterface
{

    public function findByInn(int $inn): array
    {
        return [$inn];
    }
}
