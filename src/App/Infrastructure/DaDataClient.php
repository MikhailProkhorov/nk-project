<?php

namespace App\Infrastructure;

use App\Application\Contracts\DaDataClientInterface;

class DaDataClient implements DaDataClientInterface
{
    public function __construct(private \Dadata\DadataClient $client)
    {
    }

    /**
     * @param int $inn
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function findByInn(int $inn): array
    {
        return $this->client->findById('party', $inn, 1);
    }
}
