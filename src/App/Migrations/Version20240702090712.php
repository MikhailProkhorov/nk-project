<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240702090712 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Seed Categories';
    }

    public function up(Schema $schema): void
    {
        $sql = 'INSERT INTO categories (name) VALUES ';
        $values = [];
        foreach ($this->getCategoryNames() as $name) {
            $values[] = '("' . $name . ' Category' . '")';
        }
        $sql .= implode(',', $values);
        $this->connection->executeQuery($sql);
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }

    private function getCategoryNames(): array
    {
        return [
            'First',
            'Second',
            'Third',
            'Forth',
            'Fifth'
        ];
    }
}
