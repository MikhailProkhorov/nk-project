<?php

namespace App\Application\Services;

use App\Application\Exceptions\ProductNotFoundException;
use App\Domain\Collections\ProductCollection;
use App\Domain\Contracts\Infrastructure\CategoryRepositoryInterface;
use App\Domain\Contracts\Infrastructure\ProductRepositoryInterface;
use App\Domain\DTO\CreateProduct;
use App\Domain\DTO\DeletedProduct;
use App\Domain\DTO\UpdateProduct;
use App\Domain\Models\Product;
use App\Domain\Shared\Collections\ErrorCollection;
use App\Domain\Shared\Models\Error;
use App\Exceptions\ProductNotUniqueException;
use App\Http\Requests\AbstractRequest;

class ProductService
{
    public function __construct(
        private ProductRepositoryInterface $productRepository,
        private CategoryRepositoryInterface $categoryRepository,
        private DaDataService $daDataService
    )
    {}

    public function getProductById(int $id): ?Product
    {
        return $this->productRepository->getById($id);
    }

    public function beforeSave(AbstractRequest $formRequest): ErrorCollection
    {
        $errorCollection = new ErrorCollection();
        $formRequest->validate();
        if ($formRequest->hasErrors()) {
            foreach ($formRequest->getErrors() as $error) {
                $errorCollection->add(new Error($error->getMessage()));
            }

            return $errorCollection;
        }

        if (! $this->daDataService->isINNRegistered($formRequest->getResult()->inn)) {
            $errorCollection->add(new Error('Company is not registered'));
        }

        return $errorCollection;
    }

    /**
     * @param CreateProduct $createProduct
     * @return Product
     * @throws ProductNotUniqueException
     */
    public function create(CreateProduct $createProduct): Product
    {
        if (! $this->productRepository->isUnique($createProduct->inn, $createProduct->barcode)) {
            throw new ProductNotUniqueException($createProduct->inn, $createProduct->barcode);
        }

        if ($createProduct->categories) {
            $createProduct->categories = $this->categoryRepository->findListById($createProduct->categories);
        }

        return $this->productRepository->create($createProduct);
    }

    public function findProductList(string $searchQuery = ''): ProductCollection
    {
        if (! $searchQuery) {
            return $this->productRepository->findProductsAll();
        }

        return $this->productRepository->findBySearchQuery($searchQuery);
    }

    /**
     * @param int $productId
     * @return DeletedProduct
     * @throws ProductNotFoundException
     */
    public function delete(int $productId): DeletedProduct
    {
        /** @var Product $product */
        $product = $this->productRepository->getById($productId);
        if (! $product) {
            throw new ProductNotFoundException($productId);
        }

        $this->productRepository->delete($productId);

        return new DeletedProduct(
            id: $product->id,
            name: $product->name
        );
    }

    public function update(UpdateProduct $updateProduct): Product
    {
        /** @var Product $product */
        $product = $this->productRepository->getById($updateProduct->id);
        if (! $product) {
            throw new ProductNotFoundException($updateProduct->id);
        }

        if (! $this->productRepository->isUnique($updateProduct->inn, $updateProduct->barcode)) {
            throw new ProductNotUniqueException($updateProduct->inn, $updateProduct->barcode);
        }

        return $this->productRepository->update($updateProduct);
    }
}
