<?php

namespace App\Application\Services;

use App\Application\Contracts\DaDataClientInterface;
use Throwable;

class DaDataService
{
    private array $errors;

    public function __construct(private DaDataClientInterface $daDataClient)
    {}

    public function isINNRegistered(int $inn)
    {
        try {
            return count($this->daDataClient->findByInn($inn)) > 0;
        } catch (Throwable $e) {
            $this->errors[] = $e;
        }

        return false;
    }
}
