<?php

namespace App\Application\Exceptions;

use Exception;

class ProductNotFoundException extends Exception
{
    public function __construct(int $id, $code = 0, $previous = null)
    {
        $message = sprintf('Product id: %s does not exists', $id);

        parent::__construct($message, $code, $previous);
    }
}
