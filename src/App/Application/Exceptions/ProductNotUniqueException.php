<?php

namespace App\Exceptions;

use Exception;

class ProductNotUniqueException extends Exception
{
    public function __construct(int $inn, int $barcode, $code = 0, $previouse = null)
    {
        $message = sprintf('Product with inn: %d and barcode: %d exists', $inn, $barcode);

        parent::__construct($message, $code, $previouse);
    }
}
