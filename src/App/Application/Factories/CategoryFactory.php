<?php

namespace App\Application\Factories;

use App\Domain\Collections\CategoryCollection;
use App\Domain\Models\Category;
use JetBrains\PhpStorm\NoReturn;

class CategoryFactory
{
    public function collectionFromRequest(array $requestParams): CategoryCollection
    {
        $collection = new CategoryCollection([]);
        if (isset($requestParams['categories'])) {
            foreach ($requestParams['categories'] as $category) {
                $collection->add(new Category(
                    id: $category['id'],
                    name: $category['name']
                ));
            }
        }

        return $collection;
    }
}
