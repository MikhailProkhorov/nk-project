<?php

namespace App\Application\Factories;

use App\Domain\Models\Product;
use Core\Contracts\RequestInterface;

class ProductFactory
{
    /**
     * @param RequestInterface $request
     * @return Product
     */
    public function fromRequest(array $requestData): Product
    {
        return new Product(
            id: $requestData['id'] ?? null,
            name: $requestData['name'],
            inn: $requestData['inn'],
            barcode: $requestData['barcode'],
            description: $requestData['description'],
            categories: (new CategoryFactory())->collectionFromRequest($requestData)
        );
    }
}
