<?php

namespace App\Application\Contracts;

interface DaDataClientInterface
{
    public function findByInn(int $inn): array;
}
