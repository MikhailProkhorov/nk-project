<?php

namespace App\Http\Requests;

use App\Domain\DTO\CreateProduct;
use App\Http\Validators\Constraints\EANThirteen;
use App\Http\Validators\Constraints\INN;
use Symfony\Component\Validator\Constraints;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class CreateProductFormRequest extends AbstractRequest
{
    public function __construct(
        protected array              $requestData,
        protected ValidatorInterface $validator
    )
    {
    }

    /**
     * @return CreateProduct
     */
    public function getResult(): CreateProduct
    {
        return new CreateProduct(
            name: $this->requestData['name'],
            inn: $this->requestData['inn'],
            barcode: $this->requestData['barcode'],
            description: $this->requestData['description'],
            categories: $this->requestData['categories'],
        );
    }

    protected function getConstraints(): Constraints\Collection
    {
        return new Constraints\Collection([
            'name' => new Constraints\NotBlank(),
            'inn' => [
                new Constraints\NotBlank(
                    options: null,
                    message: 'Поле ИНН должно быть заполнено',
                    allowNull: null,
                    normalizer: 'trim'
                ),
                new INN(mode: 'loose'),
            ],
            'barcode' => [
                new Constraints\NotBlank(
                    options: null,
                    message: 'Поле описание должно быть заполнено',
                    allowNull: false,
                    normalizer: 'trim'
                ),
                new EANThirteen(mode: 'loose'),
            ],
            'description' => new Constraints\NotBlank(),
            'categories' => new Constraints\Optional([
                new Constraints\Type('array')
            ]),
        ]);
    }
}
