<?php

namespace App\Http\Requests;

use App\Domain\DTO\CreateProduct;
use App\Domain\DTO\UpdateProduct;
use App\Http\Validators\Constraints\EANThirteen;
use App\Http\Validators\Constraints\INN;
use Symfony\Component\Validator\Constraints;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class UpdateProductFormRequest extends AbstractRequest
{
    public function __construct(
        protected array              $requestData,
        protected ValidatorInterface $validator
    )
    {
    }

    /**
     * @return CreateProduct
     */
    public function getResult(): UpdateProduct
    {
        return new UpdateProduct(
            id: $this->requestData['id'],
            name: $this->requestData['name'],
            inn: $this->requestData['inn'],
            barcode: $this->requestData['barcode'],
            description: $this->requestData['description'],
        );
    }

    protected function getConstraints(): Constraints\Collection
    {
        return new Constraints\Collection([
            'id' => [
                new Constraints\NotBlank(),
                new Constraints\Positive()
            ],
            'name' => new Constraints\NotBlank(),
            'inn' => [
                new Constraints\NotBlank(
                    options: null,
                    message: 'Поле ИНН должно быть заполнено',
                    allowNull: null,
                    normalizer: 'trim'
                ),
                new INN(mode: 'loose'),
            ],
            'barcode' => [
                new Constraints\NotBlank(
                    options: null,
                    message: 'Поле описание должно быть заполнено',
                    allowNull: false,
                    normalizer: 'trim'
                ),
                new EANThirteen(mode: 'loose'),
            ],
            'description' => new Constraints\NotBlank(),
        ]);
    }
}
