<?php

namespace App\Http\Requests;

use Symfony\Component\Validator\Constraints;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

abstract class AbstractRequest
{
    protected array $requestData;
    protected ValidatorInterface $validator;
    protected ?ConstraintViolationListInterface $errors = null;

    abstract public function getResult(): mixed;

    abstract protected function getConstraints(): Constraints\Collection;

    /**
     * @return array|null
     */
    public function validate(): ?array
    {
        $constraints = $this->getConstraints();
        $constraints->allowExtraFields = true;
        $constraints->allowMissingFields = true;

        $violations = $this->validator->validate(
            $this->requestData,
            $this->getConstraints(),
            new Constraints\GroupSequence(['Default', 'custom'])
        );
        if ($violations->count()) {
            $this->errors = $violations;
            return null;
        }

        return $this->requestData;
    }

    /**
     * @return ConstraintViolationListInterface
     */
    public function getErrors(): ConstraintViolationListInterface
    {
        return $this->errors;
    }

    /**
     * @return bool
     */
    public function hasErrors(): bool
    {
        return $this->errors !== null && $this->errors->count() > 0;
    }

    public function getRequestData(): array
    {
        return $this->requestData;
    }
}
