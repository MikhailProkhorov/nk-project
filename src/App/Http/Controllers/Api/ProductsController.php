<?php

namespace App\Http\Controllers\Api;

use App\Application\Services\ProductService;
use App\Domain\Collections\CategoryCollection;
use App\Domain\Contracts\ProductsFilterInterface;
use App\Domain\Shared\Models\Error;
use App\Exceptions\ProductNotUniqueException;
use App\Http\Requests\CreateProductFormRequest;
use App\Http\Requests\UpdateProductFormRequest;
use Core\Contracts\ResponseInterface;
use http\Exception\BadMethodCallException;

class ProductsController
{
    public function __construct(private ProductService $productService)
    {}

    /**
     * @param ResponseInterface $response
     * @param ProductsFilterInterface|null $filter
     * @return ResponseInterface
     */
    public function index(ResponseInterface $response, string $searchQuery = ''): ResponseInterface
    {
        $response->setBody($this->productService->findProductList($searchQuery));

        return $response;
    }

    /**
     * @param CreateProductFormRequest $formRequest
     * @param ResponseInterface $response
     * @return ResponseInterface
     */
    public function create(
        CreateProductFormRequest $formRequest,
        ResponseInterface $response
    ): ResponseInterface
    {
        $errorCollection = $this->productService->beforeSave($formRequest);
        if ($errorCollection->count() > 0) {
            $response->setStatus(400);
            /** @var Error $error */
            foreach ($errorCollection as $error) {
                $response->setError($error->getMessage());
            }

            return $response;
        }

        $createDTO = $formRequest->getResult();

        try {
            $response->setBody(
                $this->productService->create($createDTO)->jsonSerialize()
            );
        } catch (ProductNotUniqueException $e) {
            $response->setStatus(400);
            $response->setError($e->getMessage());
            return $response;
        }

        $response->setStatus(201);

        return $response;
    }

    public function update(
        UpdateProductFormRequest $formRequest,
        ResponseInterface $response,
    ): ResponseInterface
    {
        $errorCollection = $this->productService->beforeSave($formRequest);
        if ($errorCollection->count() > 0) {
            $response->setStatus(400);
            /** @var Error $error */
            foreach ($errorCollection as $error) {
                $response->setError($error->getMessage());
            }

            return $response;
        }

        $updateDTO = $formRequest->getResult();

        try {
            $response->setBody(
                $this->productService->update($updateDTO)->jsonSerialize()
            );
        } catch (ProductNotUniqueException $e) {
            $response->setStatus(400);
            $response->setError($e->getMessage());
            return $response;
        }

        $response->setStatus(200);

        return $response;
    }

    /**
     * @param CategoryCollection $categoryCollection
     * @param ResponseInterface $response
     * @return mixed
     */
    public function setCategories(CategoryCollection $categoryCollection, ResponseInterface $response): mixed
    {
        throw new BadMethodCallException('not implemented');
    }

    public function show(int $id, ResponseInterface $response): ResponseInterface
    {
        if ($product = $this->productService->getProductById($id)) {
            $response->setData($product);
        } else {
            $response->setStatus(404);
        }

        return $response;
    }

    public function delete(int $id, ResponseInterface $response): ResponseInterface
    {
        try {
            $deleted = $this->productService->delete($id);
            $response->setBody($deleted);
            return $response;
        } catch (\Throwable $e) {
            $response->setStatus(500);
            $response->setError($e->getMessage());
            return $response;
        }
    }
}
