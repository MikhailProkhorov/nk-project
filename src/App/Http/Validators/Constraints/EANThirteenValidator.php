<?php

namespace App\Http\Validators\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class EANThirteenValidator extends ConstraintValidator
{

    public function validate(mixed $value, Constraint $constraint): void
    {
        if (!$constraint instanceof EANThirteen) {
            throw new UnexpectedTypeException($constraint, EANThirteen::class);
        }

        $sumEvenIndexes = 0;
        $sumOddIndexes  = 0;
        $errorMessage = '';
        $errorCode = 0;
        $ean = $value;

        $eanAsArray = array_map('intval', str_split($ean));

        if (!$this->has13Numbers($eanAsArray)) {
            $errorMessage = 'Код должен содержать 13 цифр';
            $errorCode = 13;
        };

        for ($i = 0; $i < count($eanAsArray)-1; $i++) {
            if ($i % 2 === 0) {
                $sumOddIndexes  += $eanAsArray[$i];
            } else {
                $sumEvenIndexes += $eanAsArray[$i];
            }
        }

        $rest = ($sumOddIndexes + (3 * $sumEvenIndexes)) % 10;

        if ($rest !== 0) {
            $rest = 10 - $rest;
        }

        if (isset($eanAsArray[12]) && $rest !== $eanAsArray[12]) {
            $errorMessage = 'Код не валидный';
            $errorCode = 14;
        }

        if ($errorMessage) {
            $message = sprintf('%s :%s [code %d]', $constraint->message, $errorMessage, $errorCode);

            $this->context->buildViolation($message)
                ->setParameter('{{string}}', $value)
                ->addViolation();
        }
    }
    /**
     * @param array $ean
     * @return bool
     */
    private function has13Numbers(array $ean): bool
    {
        return count($ean) === 13;
    }
}
