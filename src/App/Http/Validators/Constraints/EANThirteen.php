<?php

namespace App\Http\Validators\Constraints;

use Symfony\Component\Validator\Constraint;


class EANThirteen extends Constraint
{
    public string $message = 'Штрик код не соответствует стандарту EAN-13';
    public string $mode = 'strict';

    public function __construct(?string $mode = null, ?string $message = null, ?array $groups = null, $payload = null)
    {
        parent::__construct([], $groups, $payload);

        $this->mode = $mode ?? $this->mode;
        $this->message = $message ?? $this->message;
    }
}
