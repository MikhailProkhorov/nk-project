<?php

namespace App\Http\Validators\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class INNValidator extends ConstraintValidator
{

    public function validate(mixed $value, Constraint $constraint): void
    {
        if (!$constraint instanceof INN) {
            throw new UnexpectedTypeException($constraint, INN::class);
        }

        $errorMessage = '';
        $errorCode = 0;
        $result = false;
        $inn = (string) $value;
        if (!$inn) {
            $errorCode = 1;
            $errorMessage = 'ИНН пуст';
        } elseif (preg_match('/[^0-9]/', $inn)) {
            $errorCode = 2;
            $errorMessage = 'ИНН может состоять только из цифр';
        } elseif (!in_array($inn_length = strlen($inn), [10, 12])) {
            $errorCode = 3;
            $errorMessage = 'ИНН может состоять только из 10 или 12 цифр';
        } else {
            $check_digit = function($inn, $coefficients) {
                $n = 0;
                foreach ($coefficients as $i => $k) {
                    $n += $k * (int) $inn[$i];
                }
                return $n % 11 % 10;
            };
            switch ($inn_length) {
                case 10:
                    $n10 = $check_digit($inn, [2, 4, 10, 3, 5, 9, 4, 6, 8]);
                    if ($n10 === (int) $inn[9]) {
                        $result = true;
                    }
                    break;
                case 12:
                    $n11 = $check_digit($inn, [7, 2, 4, 10, 3, 5, 9, 4, 6, 8]);
                    $n12 = $check_digit($inn, [3, 7, 2, 4, 10, 3, 5, 9, 4, 6, 8]);
                    if (($n11 === (int) $inn[10]) && ($n12 === (int) $inn[11])) {
                        $result = true;
                    }
                    break;
            }
            if (!$result) {
                $errorCode = 4;
                $errorMessage = 'Неправильное контрольное число';
            }
        }
        if ($errorMessage) {
            $message = sprintf('%s :%s [code %d]', $constraint->message, $errorMessage, $errorCode);

            $this->context->buildViolation($message)
                ->setParameter('{{string}}', $value)
                ->addViolation();
        }
    }
}
