<?php

namespace Core;

use Core\Contracts\ResponseInterface;
use Psr\Http\Message\ResponseInterface as PsrResponseInterface;

class Response implements ResponseInterface
{
    private mixed $body = null;
    private int $status = 200;
    private array $headers = [
        'Content-Type: application/json; charset=utf-8'
    ];
    private string $version = '1.1';
    private ?string $reason = null;

    public function __construct()
    {
    }

    public function getResult(): PsrResponseInterface
    {
        return new \GuzzleHttp\Psr7\Response(
            status: $this->status,
            headers: $this->headers,
            body: $this->body,
            version: $this->version,
            reason: $this->reason
        );
    }

    /**
     * @return mixed
     */
    public function getBody(): mixed
    {
        return $this->body;
    }

    /**
     * @return false|string
     */
    public function toJson(): false|string
    {
        return json_encode($this->body);
    }

    public function setData(mixed $data): static
    {
        $this->setBody([
            'data' => $data,
            'status' => $this->getStatus(),
        ]);

        return $this;
    }

    /**
     * @param $body
     * @return $this
     */
    public function setBody($body): static
    {
        $this->body =  $body;

        return $this;
    }

    public function setError(string $error): static
    {
        $this->body['data'] = null;
        $this->body['errors'][] = $error;
        $this->body['status'] = $this->getStatus();

        return $this;
    }

    /**
     * @param int $status
     * @return $this
     */
    public function setStatus(int $status): static
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param array $header
     * @return $this
     */
    public function setHeaders(array $headers): static
    {
        $this->headers = $headers;

        return $this;
    }

    /**
     * @return array|string[]
     */

    public function getHeaders(): array
    {
        return $this->headers;
    }

    public function setVersion(string $version): static
    {
        $this->version = $version;

        return $this;
    }
}
