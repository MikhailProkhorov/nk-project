<?php

namespace Core;

use Core\Contracts\RequestInterface;
use GuzzleHttp\Psr7\ServerRequest;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\StreamInterface;
use Psr\Http\Message\UriInterface;

class Request implements RequestInterface
{
    public function __construct(private ServerRequestInterface $serverRequest)
    {
        $this->serverRequest = ServerRequest::fromGlobals();
    }

    /**
     * @return StreamInterface
     */
    public function getBody(): StreamInterface
    {
        return $this->serverRequest->getBody();
    }

    /**
     * @return array
     */
    public function getQueryParams(): array
    {
        return $this->serverRequest->getQueryParams();
    }

    /**
     * @return UriInterface
     */
    public function getUri(): UriInterface
    {
        return $this->serverRequest->getUri();
    }

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $this->serverRequest->getMethod();
    }

    /**
     * @return array
     */
    public function getRequestData(): array
    {
        $body = json_decode($this->getBody()->read($this->getBody()->getSize()), true);
        $queryParams = $this->getQueryParams();
        if ($body && $queryParams) {
            return array_merge($body, $queryParams);
        } elseif ($body) {
            return $body;
        } elseif ($queryParams) {
            return $queryParams;
        }

        return [];
    }
}
