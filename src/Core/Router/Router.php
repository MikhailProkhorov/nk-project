<?php

namespace Core\Router;

use ArrayObject;
use Core\Contracts\RouterInterface;
use Core\Router\Exceptions\MethodNotAllowed;
use Core\Router\Exceptions\RouteNotFoundException;
use Psr\Http\Message\ServerRequestInterface;

final class Router implements RouterInterface
{
    private const NO_ROUTE = 404;
    private const METHOD_NOT_ALLOWED = 405;
    private ArrayObject $routes;
    private UrlGenerator $urlGenerator;

    /**
     * @param array<Route> $routes
     * @param string $defaultUri
     */
    public function __construct(array $routes = [], string $defaultUri = 'http://localhost')
    {
        $this->routes = new ArrayObject();
        $this->urlGenerator = new UrlGenerator($this->routes, $defaultUri);
        foreach ($routes as $route) {
            $this->add($route);
        }
    }



    /**
     * @param Route $route
     * @return self
     */
    public function add(Route $route): self
    {
        $this->routes->offsetSet($route->getName(), $route);
        return $this;
    }

    /**
     * @param ServerRequestInterface $serverRequest
     * @return Route
     * @throws MethodNotAllowed
     * @throws RouteNotFoundException
     */
    public function match(ServerRequestInterface $serverRequest): Route
    {
        return $this->matchFromPath($serverRequest->getUri()->getPath(), $serverRequest->getMethod());
    }

    /**
     * @param string $path
     * @param string $method
     * @throws MethodNotAllowed
     * @throws RouteNotFoundException
     * @return Route
     */
    public function matchFromPath(string $path, string $method): Route
    {
        /**
         * @var Route $route
         */
        foreach ($this->routes as $route) {
            if ($route->match($path) === false) {
                continue;
            }

            if (! $route->getMethods()->hasMethod(Methods::tryFrom($method))) {
                throw new MethodNotAllowed(
                    'Method Not Allowed : ' . $method,
                    self::METHOD_NOT_ALLOWED
                );
            }
            return $route;
        }

        throw new RouteNotFoundException($path, $method);
    }

    /**
     * Generate a URI based on the provided name, parameters, and settings.
     *
     * @param string $name The name used for generating the URI.
     * @param array $parameters An array of parameters to be included in the URI.
     * @param bool $absoluteUrl Whether the generated URI should be an absolute URL.
     * @return string The generated URI.
     */
    public function generateUri(string $name, array $parameters = [], bool $absoluteUrl = false): string
    {
        return $this->urlGenerator->generate($name, $parameters, $absoluteUrl);
    }

    public function getUrlGenerator(): UrlGenerator
    {
        return $this->urlGenerator;
    }
}
