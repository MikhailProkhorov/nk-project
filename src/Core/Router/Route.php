<?php

namespace Core\Router;

use InvalidArgumentException;

final class Route
{
    private array $vars = [];

    public function __construct(
        private readonly string $name,
        private readonly string $path,
        private readonly array  $parameters = [],
        private MethodsCollection $methods = new MethodsCollection(),
    )
    {
        if ($methods->count() === 0) {
            $this->methods->add(Methods::GET);
        }
    }

    /**
     * @param string $path
     * @param Methods $method
     * @return bool
     */
    public function match(string $path): bool
    {
        $regex = str_replace('/', '\\/', $this->getPath());

        foreach ($this->getVarsNames() as $variable) {
            $varName = trim($variable, '{\}');
            $regex = str_replace($variable . '}', '(?P<' . $varName . '>[^\/]++)', $regex);
        }
        /*
        if (preg_match('~update~', $regex)) {
            var_dump(self::trimPath($path));
            var_dump($regex);
            var_dump('#^' . $regex . '(\/)?$#sD');
            preg_match('#^' . $regex . '(\/)?$#sD', self::trimPath($path), $matches);
            var_dump($matches);
            die;
        }
        */

        if (! preg_match('#^' . $regex . '(\/)?$#sD', self::trimPath($path), $matches)) {
            return false;
        }

        $values = array_filter($matches, static function ($key) {
            return is_string($key);
        }, ARRAY_FILTER_USE_KEY);

        foreach ($values as $key => $value) {
            if (array_key_exists($key, $this->vars) && !preg_match('/^'.$this->vars[$key].'$/', $value)) {
                return false;
            }
            $this->vars[$key] = $value;
        }

        return true;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @return array
     */
    public function getParameters(): array
    {
        return $this->parameters;
    }

    /**
     * @return MethodsCollection
     */
    public function getMethods(): MethodsCollection
    {
        return $this->methods;
    }

    /**
     * @return array
     */
    public function getVarsNames()//: array
    {
        preg_match_all('~\{([^}]+)*~', $this->path, $matches);
        return reset($matches) ?? [];
    }

    /**
     * @return bool
     */
    public function hasVars(): bool
    {
        return $this->getVarsNames() !== [];
    }

    /**
     * @return array
     */
    public function getVars(): array
    {
        return $this->vars;
    }

    /**
     * @param string $path
     * @return string
     */
    public static function trimPath(string $path): string
    {
        return '/' . rtrim(ltrim(trim($path), '/'), '/');
    }
}
