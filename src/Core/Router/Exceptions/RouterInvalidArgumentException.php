<?php

namespace Core\Router\Exceptions;

use InvalidArgumentException;

class RouterInvalidArgumentException extends InvalidArgumentException
{}
