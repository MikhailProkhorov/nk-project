<?php

namespace Core\Router\Exceptions;

use Exception;

class RouteNotFoundException extends Exception
{
    public function __construct(string $method, string $path, $code = 0, $previous = null)
    {
        $message = sprintf('No route found for %s: %s', $method, $path);
        parent::__construct($message, $code, $previous);
    }
}
