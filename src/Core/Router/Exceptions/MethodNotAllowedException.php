<?php

namespace Core\Router\Exceptions;

use Exception;

class MethodNotAllowed extends Exception
{
}
