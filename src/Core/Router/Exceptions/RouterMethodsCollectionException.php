<?php

namespace Core\Router\Exceptions;

use InvalidArgumentException;

class RouterMethodsCollectionException extends InvalidArgumentException
{}
