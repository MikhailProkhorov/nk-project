<?php

namespace Core\Router;

use ArrayAccess;
use Core\Router\Exceptions\RouterInvalidArgumentException;

final readonly class UrlGenerator
{
    /**
     * @param ArrayAccess<Route> $routes
     */
    public function __construct(
        private ArrayAccess $routes
    )
    {}

    /**
     * @param string $name
     * @param array $parameters
     * @return string
     */
    public function generate(string $name, array $parameters): string
    {
        if ($this->routes->offsetExists($name) === false) {
            throw new RouterInvalidArgumentException(
                sprintf('Unknown %s name route', $name)
            );
        }

        $route = $this->routes[$name];
        if ($route->hasVars() && $parameters === []) {
            throw new RouterInvalidArgumentException(
                sprintf('%s route need parameters: %s', $name, implode(',', $route->getVarsNames()))
            );
        }

        return self::resolveUrl($route, $parameters);
    }

    /**
     * @param Route $route
     * @param array $parameters
     * @return array|string|string[]
     */
    private static function resolveUrl(Route $route, array $parameters): array|string
    {
        $uri = $route->getPath();
        foreach ($route->getVarsNames() as $variable) {
            $varName = trim($variable, '{\}');
            if (array_key_exists($varName, $parameters) === false) {
                throw new RouterInvalidArgumentException(
                    sprintf('%s not found in parameters to generate url', $varName)
                );
            }

            $uri = str_replace($variable, $parameters[$varName], $uri);
        }

        return $uri;
    }
}
