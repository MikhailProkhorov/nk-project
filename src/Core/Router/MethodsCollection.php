<?php

namespace Core\Router;

use ArrayAccess;
use Core\Router\Exceptions\RouterMethodsCollectionException;
use Countable;
use http\Exception\InvalidArgumentException;

class MethodsCollection implements ArrayAccess, Countable
{

    /** @var array|Methods[] */
    private array $methods = [];

    /**
     * @param Methods $method
     * @return $this
     */
    public function add(Methods $method): static
    {
        $this->methods[] = $method;

        return $this;
    }

    /**
     * @param $offset
     * @param $value
     * @return void
     */
    public function offsetSet($offset, $value): void
    {
        if (! $value instanceof Methods) {
            throw new RouterMethodsCollectionException('Value must be instance of Methods');
        }

        if (is_null($offset)) {
            $this->methods[] = $value;
        } else {
            $this->methods[$offset] = $value;
        }
    }

    /**
     * @param $offset
     * @return bool
     */
    public function offsetExists($offset): bool
    {
        return isset($this->methods[$offset]);
    }

    /**
     * @param $offset
     * @return void
     */
    public function offsetUnset($offset): void
    {
        unset($this->methods[$offset]);
    }

    /**
     * @param $offset
     * @return Methods|null
     */
    public function offsetGet($offset): ?Methods
    {
        return $this->methods[$offset] ?? null;
    }

    public function count(): int
    {
        return count($this->methods);
    }

    public function hasMethod(Methods $method): bool
    {
        foreach ($this->methods as $methodItem) {
            if ($methodItem === $method) {
                return true;
            }
        }

        return false;
    }
}
