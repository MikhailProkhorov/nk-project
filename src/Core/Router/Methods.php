<?php

namespace Core\Router;

enum Methods: string
{
    case GET = 'GET';
    case POST = 'POST';
    case PATCH = 'PATCH';
    case DELETE = 'DELETE';
    case PUT = 'PUT';
}
