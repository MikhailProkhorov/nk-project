<?php

namespace Core;

use Closure;
use Core\Contracts\RequestInterface;
use Core\Contracts\RouterInterface;
use Core\DI\DI;
use Core\DI\Exceptions\DependencyNotFoundException;
use Core\Router\Exceptions\RouteNotFoundException;
use Core\Router\Router;
use Doctrine\Migrations\DependencyFactory;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMSetup;
use Dotenv\Dotenv;
use GuzzleHttp\Psr7\ServerRequest;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use Throwable;

class App
{
    public const string PARAMETERS_DI = 'app.parameters';
    private string $routesPath;
    private string $configPath;
    private string $parametersPath;
    private $environment;
    private ContainerInterface $di;
    private RequestInterface $request;

    private RouterInterface $router;
    private array $requestData = [];

    private static App $instance;

    /**
     * @throws DependencyNotFoundException
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __construct()
    {
        try {
            $this->routesPath = $this->getRoutesPath();
            $this->configPath = $this->getConfigPath();
            $this->parametersPath = $this->getParametersPath();
            $config = require($this->configPath);
            $this->di = new DI($config['di']);
            $this->request = new Request(ServerRequest::fromGlobals());
            $this->requestData = $this->request->getRequestData();
            $this->di->set(EntityManagerInterface::class, $this->getEntityManager());
        } catch(Throwable $e) {
            var_dump($e->getMessage());
            var_dump($e->getTraceAsString());
            die;
        }
    }

    /**
     * @return void
     * @throws RouteNotFoundException
     */

    public function init(): void
    {
        try {
            $parameters = require($this->parametersPath);
            $this->di->set(static::PARAMETERS_DI, function () use ($parameters) {
                return $parameters;
            });
            $routerConfig = require($this->routesPath);
            $this->router = new Router($routerConfig);
            $this->dispatch();
        } catch(Throwable $e) {
            var_dump($e->getMessage());
            die;
        }
    }

    /**
     * @return Closure
     */
    private function getEntityManager(): Closure
    {
        return function () {
            $appConfig = require $this->configPath;
            $connection = require dirname(__DIR__) . '/configs/db.php';
            return new EntityManager(
                $connection,
                ORMSetup::createAttributeMetadataConfiguration(
                    paths: [$appConfig['app']['entityPaths']],
                    isDevMode: $appConfig['app']['isDevMode']
                )
            );

        };
    }

    /**
     * @throws RouteNotFoundException
     */
    private function dispatch(): void
    {
        try {
            $route = $this->router->matchFromPath(
                $this->request->getUri()->getPath(),
                $this->request->getMethod()
            );
            $parameters = $route->getParameters();
            if ($this->requestData) {
                $arguments = array_merge($this->requestData, $route->getVars());
            } else {
                $arguments = $route->getVars();
            }
            $controllerName = $parameters[0];
            $controller = $this->di->build($controllerName);
            $actionName = $parameters[1] ?? null;
            $actionParameters = array_filter(
                $this->di->getActionParameters($controller, $actionName),
                function ($param) {
                    return ! empty($param);
                }
            );

            if (!is_callable($controller)) {
                $controller = [$controller, $actionName];
            }

            /** @var Response $response */
            $response = $controller(...array_values(array_merge($actionParameters, $arguments)));
            foreach ($response->getHeaders() as $header) {
                header($header);
            }
            http_response_code($response->getStatus());
            echo $response->toJson();
        } catch (RouteNotFoundException $e) {
            http_response_code($e->getcode());
            echo $e->getMessage();
        } catch (Throwable $e) {
            $this->fail($e);
        }
    }

    /**
     * @return DI|ContainerInterface
     */
    public function getDi(): DI|ContainerInterface
    {
        return $this->di;
    }

    /**
     * @return string
     */
    protected function getRoutesPath(): string
    {
        return dirname(__DIR__) . '/routes.php';
    }

    /**
     * @return string
     */
    protected function getConfigPath(): string
    {
        return dirname(__DIR__) . '/configs/config.php';
    }

    protected function getParametersPath(): string
    {
        return dirname(__DIR__) . '/configs/parameters.php';
    }

    private function fail(Throwable $e)
    {
        var_dump($e->getMessage());
        $response = new Response();
        foreach ($response->getHeaders() as $header) {
            header($header);
        }
        $response->setError($e->getMessage());
        $response->setData($e->getTrace());
        http_response_code(500);

        echo $response->toJson();
    }
}
