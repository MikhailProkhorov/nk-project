<?php

namespace Core\Contracts;

use Psr\Http\Message\StreamInterface;
use Psr\Http\Message\UriInterface;

interface RequestInterface
{
    public function getBody(): StreamInterface;

    public function getQueryParams(): array;

    public function getUri(): UriInterface;

    public function getMethod(): string;

    public function getRequestData(): array;
}
