<?php

namespace Core\Contracts;

use Core\Router\Exceptions\MethodNotAllowed;
use Core\Router\Exceptions\RouteNotFoundException;
use Core\Router\Route;
use InvalidArgumentException;
use Psr\Http\Message\ServerRequestInterface;

interface RouterInterface
{
    /**
     * @param ServerRequestInterface $serverRequest
     * @return Route
     * @throws RouteNotFoundException
     * @throws MethodNotAllowed
     */
    public function match(ServerRequestInterface $serverRequest): Route;

    /**
     * @param string $path
     * @param string $method
     * @return Route
     * @throws RouteNotFoundException
     * @throws MethodNotAllowed
     */
    public function matchFromPath(string $path, string $method): Route;

    /**
     * @param string $name
     * @param array $parameters
     * @return string
     * @throws InvalidArgumentException
     */
    public function generateUri(string $name, array $parameters = []): string;
}
