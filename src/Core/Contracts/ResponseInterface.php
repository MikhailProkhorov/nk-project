<?php

namespace Core\Contracts;

interface ResponseInterface
{
    public function getBody(): mixed;

    public function setBody(mixed $body): static;

    public function setStatus(int $status): static;

    public function getStatus(): int;

    public function setHeaders(array $headers): static;

    public function getHeaders(): array;

    public function setVersion(string $version): static;

    public function setData(mixed $data): static;

    public function setError(string $errorMessage): static;
}
