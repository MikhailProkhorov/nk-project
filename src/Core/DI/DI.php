<?php

namespace Core\DI;

use Closure;
use Core\DI\Exceptions\DependencyNotFoundException;
use Core\DI\Exceptions\DINotInstantiableException;
use Core\DI\Exceptions\DITargetNotExistsException;
use Core\DI\Exceptions\UnresolvableDependencyException;
use Psr\Container\ContainerInterface;
use Reflection;
use ReflectionClass;
use ReflectionException;
use ReflectionNamedType;
use Throwable;

class DI implements ContainerInterface
{
    private array $definitions = [];
    private array $resolvedEntries = [];

    public function __construct(array $definitions = [])
    {
        $this->definitions = array_merge(
            $definitions,
            [ContainerInterface::class => $this]
        );
    }

    /**
     * @param string $id
     * @param callable $factory
     * @return void
     */
    public function set(string $id, callable $factory): void
    {
        $this->definitions[$id] = $factory;
    }

    /**
     * @param string $id
     * @return mixed
     * @throws DependencyNotFoundException
     */
    public function get(string $id): mixed
    {
        if (! $this->has($id)) {
            throw new DependencyNotFoundException($id);
        }

        if (array_key_exists($id, $this->resolvedEntries)) {
            return $this->resolvedEntries[$id];
        }

        $value = $this->definitions[$id];
        if ($value instanceof Closure) {
            $value = $value($this);
        }

        $this->resolvedEntries[$id] = $value;

        return $value;
    }

    /**
     * @param string $class
     * @return mixed|object|string|null
     * @throws ReflectionException
     * @throws UnresolvableDependencyException
     */
    public function build(string $class): mixed
    {
        if ($this->has($class)) {
            return $this->get($class);
        }
        try {
            $reflection = new ReflectionClass($class);
        } catch (ReflectionException $e) {
            throw new DITargetNotExistsException($class);
        }

        if (! $reflection->isInstantiable()) {
            throw new DINotInstantiableException($class);
        }

        $constructor = $reflection->getConstructor();

        if ($constructor === null) {
            return new $class;
        }

        $parameters = $constructor->getParameters();
        $dependencies = [];

        foreach ($parameters as $parameter) {
            $type = $parameter->getType();

            if (! $type instanceof ReflectionNamedType || $type->isBuiltin()) {
                if ($parameter->isDefaultValueAvailable()) {
                    $dependencies[] = $parameter->getDefaultValue();
                } elseif ($parameter->isVariadic()) {
                    $dependencies[] = [];
                } else {
                    throw new UnresolvableDependencyException($parameter);
                }
            }

            $name = $parameter->getName();

            try {
                $dependency = $this->get($name);
                $dependencies[] = $dependency;
            } catch(Throwable $e) {
                if ($parameter->isOptional()) {
                    $dependencies[] = $parameter->getDefaultValue();
                } else {
                    $dependency = $this->build($parameter->getType()->getName());
                    $dependencies[] = $dependency;
                }
            }
        }

        return $reflection->newInstanceArgs($dependencies);
    }

    /**
     * @param $class
     * @param string $action
     * @return array
     * @throws DependencyNotFoundException
     * @throws ReflectionException
     * @throws UnresolvableDependencyException
     */
    public function getActionParameters($class, string $action): array
    {
        $actionParameters = [];
        $reflection = new ReflectionClass($class);
        $method = $reflection->getMethod($action);
        $parameters = $method->getParameters();
        foreach ($parameters as $parameter) {
            $type = $parameter->getType();
            if (! $type) {
                continue;
            }
            if (! ($type instanceof ReflectionNamedType || $type->isBuiltin())) {
                if ($parameter->isDefaultValueAvailable()) {
                    $dependencies[] = $parameter->getDefaultValue();
                    $actionParameters[] = $parameter->getDefaultValue();
                } elseif ($parameter->isVariadic()) {
                    $dependencies[] = [];
                    $actionParameters[] = [];
                } else {
                    throw new UnresolvableDependencyException($parameter);
                }
            }

            $name = $parameter->getName();

            try {
                $dependency = $this->get($name);
                $dependencies[] = $dependency;
            } catch(Throwable $e) {
                if ($parameter->isOptional()) {
                    $dependencies[] = $parameter->getDefaultValue();
                    $actionParameters[] = $parameter->getDefaultValue();
                } elseif (! $parameter->getType()->isBuiltin()) {
                    $dependency = $this->get($parameter->getType()->getName());
                    $dependencies[] = $dependency;
                    $actionParameters[] = $dependency;
                } else {
                    $dependencies[] = [];
                }
            }
        }
        return $actionParameters;
    }

    public function has(string $id): bool
    {
        return array_key_exists($id, $this->definitions) || array_key_exists($id, $this->resolvedEntries);
    }
}
