<?php

namespace Core\DI\Exceptions;

use Exception;
use Psr\Container\NotFoundExceptionInterface;

class DependencyNotFoundException extends Exception implements NotFoundExceptionInterface
{
    /**
     * @param $target
     * @param $code
     * @param $previous
     */
    public function __construct($target, $code = 0, $previous = null)
    {
        $message = sprintf('Target binding [%s] does not exist.', $target);
        parent::__construct($message, $code, $previous);
    }
}
