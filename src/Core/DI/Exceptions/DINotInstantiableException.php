<?php

namespace Core\DI\Exceptions;

use InvalidArgumentException;

class DINotInstantiableException extends InvalidArgumentException
{

    /**
     * @param string $class
     * @param int $code
     * @param null $previous
     */
    public function __construct(string $class, $code = 0, $previous = null)
    {
        $message = sprintf('Target [%s] is not instantiable.', $class);
        parent::__construct($message, $code, $previous);
    }
}
