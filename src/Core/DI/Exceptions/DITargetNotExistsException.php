<?php

namespace Core\DI\Exceptions;

class DITargetNotExistsException extends \InvalidArgumentException
{
    /**
     * @param $target
     * @param $code
     * @param $previous
     */
    public function __construct($target, $code = 0, $previous = null)
    {
        $message = sprintf('Target class [%s] does not exist.', $target);
        parent::__construct($message, $code, $previous);
    }
}
