<?php

namespace Core\DI\Exceptions;

use Exception;
use ReflectionParameter;

class UnresolvableDependencyException extends Exception
{
    /**
     * @param ReflectionParameter $parameter
     * @param $code
     * @param $previous
     */
    public function __construct(ReflectionParameter $parameter, $code = 0, $previous = null)
    {
        $message = sprintf(
            'Unresolvable dependency [%s] in class {%s}',
            $parameter,
            $parameter->getDeclaringClass()->getName()
        );

        parent::__construct($message, $code, $previous);
    }
}
