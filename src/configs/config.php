<?php

use App\Application\Contracts\DaDataClientInterface;
use App\Application\Factories\ProductFactory;
use App\Application\Services\DaDataService;
use App\Application\Services\ProductService;
use App\Domain\Contracts\Infrastructure\CategoryRepositoryInterface;
use App\Domain\Contracts\Infrastructure\ProductRepositoryInterface;
use App\Domain\DTO\CreateProduct;
use App\Domain\Models\Product;
use App\Http\Requests\CreateProductFormRequest;
use App\Http\Requests\UpdateProductFormRequest;
use App\Infrastructure\Entities\Product as ProductEntity;
use App\Infrastructure\Entities\Category as CategoryEntity;
use Core\Contracts\RequestInterface;
use Core\Contracts\ResponseInterface;
use Core\Request;
use Core\Response;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Psr7\ServerRequest;
use Psr\Container\ContainerInterface;
use Symfony\Component\Validator\Validation;

return [
    'app' => [
        'isDevMode' => true,
        'entityPaths' => [
            dirname(__DIR__) . '/App/Infrastructure/Entities',
        ],
    ],
    'di' => [
        RequestInterface::class => static fn() => new Request(ServerRequest::fromGlobals()),
        ResponseInterface::class => static fn(ContainerInterface $di) => new Response(),
        CreateProductFormRequest::class => static fn(ContainerInterface $di) =>
            new CreateProductFormRequest(
                $di->get(RequestInterface::class)->getRequestData(),
                Validation::createValidator()
            ),
        UpdateProductFormRequest::class => static fn(ContainerInterface $di) =>
        new UpdateProductFormRequest(
            $di->get(RequestInterface::class)->getRequestData(),
            Validation::createValidator()
        ),
        Product::class => static fn(ContainerInterface $di) =>
            (new ProductFactory())->fromRequest($di->get(RequestInterface::class)),
        CreateProduct::class => static function (ContainerInterface $di) {
            $product = (new ProductFactory())->fromRequest($di->get(RequestInterface::class)->getRequestData());
            return new CreateProduct(
                name: $product->name,
                inn: $product->inn,
                barcode: $product->barcode,
                description: $product->description,
            );
        },
        ProductRepositoryInterface::class => static function(ContainerInterface $di) {
            /** @var EntityManagerInterface $em */
            $em = $di->get(EntityManagerInterface::class);
            return $em->getRepository(ProductEntity::class);
        },
        CategoryRepositoryInterface::class => static function (ContainerInterface $di) {
            /** @var EntityManagerInterface $em */
            $em = $di->get(EntityManagerInterface::class);
            return $em->getRepository(CategoryEntity::class);
        },
        DaDataClientInterface::class => static function(ContainerInterface $di) {
            return new \App\Infrastructure\DummyDataClient();
//            $parameters = $di->get(App::PARAMETERS_DI);
            /*
            return new DaDataClient(
                client: new \Dadata\DadataClient(token: $parameters['DADATA_KEY'], secret: null)
            );
            */
        },
        DaDataService::class => static fn(ContainerInterface $di) => new DaDataService($di->get(DaDataClientInterface::class)),
        ProductService::class => static fn(ContainerInterface $di) =>
            new ProductService(
                $di->get(ProductRepositoryInterface::class),
                $di->get(CategoryRepositoryInterface::class),
                $di->get(DaDataService::class)
            ),
    ],
];
