<?php

namespace tests\Domain\Collections;

use App\Domain\Collections\CategoryCollection;
use App\Domain\Models\Category;
use PHPUnit\Framework\TestCase;

class CategoryCollectionTest extends TestCase
{
    protected $collection;

    public function setUp(): void
    {
        $this->collection = new CategoryCollection([
            new Category(id: 1, name: 'first'),
            new Category(id: 2, name: 'second'),
            new Category(id: 3, name: 'third'),
            new Category(id: 4, name: 'forth'),
            new Category(id: 5, name: 'fifth'),
        ]);
    }

    public function testClassIteratorAggregateInstance()
    {
        $this->assertInstanceOf(\IteratorAggregate::class, $this->collection);
    }

    public function testAddCategory()
    {
        $previousLength = 0;
        foreach ($this->collection as $item) {
            $previousLength += 1;
        }

        $this->collection->add(new Category(id: 7, name: 'seventh'));

        $afterLength = 0;
        foreach ($this->collection as $item) {
            $afterLength += 1;
        }

        $this->assertEquals($previousLength + 1, $afterLength);
    }

    public function testJsonSerializable()
    {
        $this->assertJson(json_encode($this->collection));
        $this->assertInstanceOf(\JsonSerializable::class, $this->collection);
    }
}
