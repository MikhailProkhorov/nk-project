<?php

namespace tests\Domain\Collections;

use App\Domain\Collections\ProductCollection;
use App\Domain\Models\Product;
use PHPUnit\Framework\TestCase;

class ProductCollectionTest extends TestCase
{
    protected $collection;

    public function setUp(): void
    {
        $this->collection = new ProductCollection([
            new Product(
                id: 11,
                name: 'Test Name',
                inn: 1233229837,
                barcode: 8462939473017,
                description: 'Test Description',
            ),
            new Product(
                id: 12,
                name: 'Test 2 Name',
                inn: 1233229832,
                barcode: 8462939473013,
                description: 'Test Description',
            ),
            new Product(
                id: 13,
                name: 'Test 3 Name',
                inn: 1233229835,
                barcode: 7462939473017,
                description: 'Test Description',
            ),
        ]);
    }

    public function testClassIteratorAggregateInstance()
    {
        $this->assertInstanceOf(\IteratorAggregate::class, $this->collection);
    }

    public function testAdd()
    {
        $previousLength = 0;
        foreach ($this->collection as $item) {
            $previousLength += 1;
        }
        $this->collection->add(new Product(
            id: 42,
            name: 'Test Name',
            inn: 1233229837,
            barcode: 8462939473017,
            description: 'Test Description',
        ));
        $afterLength = 0;
        foreach ($this->collection as $item) {
            $afterLength += 1;
        }

        self::assertEquals($previousLength + 1, $afterLength);
    }

    public function testJsonSerializable()
    {
        $this->assertJson(json_encode($this->collection));
        $this->assertInstanceOf(\JsonSerializable::class, $this->collection);
    }
}
