<?php

namespace tests\Domain\Models;

use App\Domain\Collections\CategoryCollection;
use App\Domain\Models\Category;
use App\Domain\Models\Product;
use PHPUnit\Framework\TestCase;

class ProductTest extends TestCase
{
    protected $product;
    protected $categories;

    public function setUp(): void
    {
        $this->product = new Product(
            id: 11,
            name: 'Test Name',
            inn: 1233229837,
            barcode: 8462939473017,
            description: 'Test Description',
        );

        $this->categories = new CategoryCollection([
            new Category(id: 1, name: 'first'),
            new Category(id: 2, name: 'second'),
            new Category(id: 3, name: 'third'),
            new Category(id: 4, name: 'forth'),
            new Category(id: 5, name: 'fifth'),
        ]);
    }

    public function testGetId()
    {
        $this->assertIsInt($this->product->getId());
    }

    public function testCategories()
    {
        $this->assertNull($this->product->categories);
        $this->product->categories = $this->categories;
        $this->assertInstanceOf(CategoryCollection::class, $this->product->categories);
    }

    public function testJsonSerializable()
    {
        $this->assertJson(json_encode($this->product));
        $this->assertInstanceOf(\JsonSerializable::class, $this->product);
        $this->assertNull($this->product->categories);
        $this->product->categories = $this->categories;
        $this->assertInstanceOf(\JsonSerializable::class, $this->product->categories);

    }

    public function testCategoriesSerializable()
    {

        $this->product->categories = $this->categories;
        $this->assertJson(json_encode($this->product->categories));
    }
}
