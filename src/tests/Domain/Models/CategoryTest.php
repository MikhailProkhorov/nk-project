<?php

namespace test\Domain\Models;

use App\Domain\Models\Category;
use PHPUnit\Framework\TestCase;

class CategoryTest extends TestCase
{
    protected $category;

    public function setUp(): void
    {
        $this->category = new Category(id: 1, name: 'Test Category Name');
    }

    public function testCategoryName()
    {
        $this->assertIsString($this->category->name);
    }

    public function testCategoryId()
    {
        $this->assertIsInt($this->category->id);
    }

    public function testCategoryJsonSerializable()
    {

        $this->assertJson(json_encode($this->category));
    }
}
