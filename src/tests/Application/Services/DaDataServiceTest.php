<?php

namespace test\Application\Services;

use App\Application\Services\DaDataService;
use App\Infrastructure\DaDataClient;
use PHPUnit\Framework\MockObject\Exception;
use PHPUnit\Framework\TestCase;

class DaDataServiceTest extends TestCase
{
    /**
     * @throws Exception
     */
    public function testIsINNRegisteredFalse()
    {
        $client = $this->createStub(DaDataClient::class);
        $client->method('findByInn')
            ->willReturn([]);

        $service = new DaDataService($client);
        $this->assertFalse($service->isINNRegistered(12321));

    }

    /**
     * @throws Exception
     */
    public function testIsINNRegisteredTrue()
    {
        $client = $this->createStub(DaDataClient::class);
        $client->method('findByInn')
            ->willReturn([1, 2, 3]);
        $service = new DaDataService($client);
        $this->assertTrue($service->isINNRegistered(123321));
    }
}

