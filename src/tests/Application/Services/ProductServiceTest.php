<?php

namespace tests\Application\Services;

use App\Application\Contracts\DaDataClientInterface;
use App\Application\Services\DaDataService;
use App\Application\Services\ProductService;
use App\Domain\Collections\CategoryCollection;
use App\Domain\Collections\ProductCollection;
use App\Domain\Contracts\Infrastructure\CategoryRepositoryInterface;
use App\Domain\Contracts\Infrastructure\ProductRepositoryInterface;
use App\Domain\DTO\CreateProduct;
use App\Domain\DTO\DeletedProduct;
use App\Domain\DTO\UpdateProduct;
use App\Domain\Models\Category;
use App\Domain\Models\Product;
use App\Domain\Shared\Collections\ErrorCollection;
use App\Exceptions\ProductNotUniqueException;
use App\Http\Requests\CreateProductFormRequest;
use App\Http\Requests\UpdateProductFormRequest;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\Validation;

class ProductServiceTest extends TestCase
{
    protected $service;
    protected $validInn;
    protected $invalidInn;
    protected $validBarcode;
    protected $invalidBarcode;

    public function setUp(): void
    {
        $this->validInn = 7251621782;
        $this->invalidInn = 72516217;
        $this->validBarcode = 9780201379624;
        $this->invalidBarcode = 9780201379627;

        $defaultProduct = new Product(
            id: 1,
            name: 'default name',
            inn: 1111111111,
            barcode: 1222222222223,
            description: 'default description'
        );
        $defaultCollection = new ProductCollection([
            $defaultProduct,
            new Product(
                id: 2,
                name: 'default name',
                inn: 2222222222,
                barcode: 13333333333334,
                description: 'default description'
            ),
        ]);
        $categoryCollection = new CategoryCollection([
            new Category(id: 1, name: 'first'),
            new Category(id: 2, name: 'second'),
            new Category(id: 3, name: 'third'),
            new Category(id: 4, name: 'forthG'),
        ]);
        $productRepository = $this->createStub(ProductRepositoryInterface::class);
        $productRepository->method('getById')
            ->willReturn($defaultProduct);
        $productRepository->method('create')
            ->willReturn($defaultProduct);
        $productRepository->method('isUnique')
            ->willReturn(true);
        $productRepository->method('findProductsAll')
            ->willReturn($defaultCollection);
        $productRepository->method('findBySearchQuery')
            ->willReturn($defaultCollection);
        $productRepository->method('update')
            ->willReturn($defaultProduct);

        $categoryRepository = $this->createStub(CategoryRepositoryInterface::class);
        $categoryRepository->method('findListById')
            ->willReturn($categoryCollection);
        $daDataClient = $this->createStub(DaDataClientInterface::class);
        $daDataClient->method('findByInn')
            ->willReturn([1, 2, 4]);
        $datDataService = new DaDataService($daDataClient);

        $this->service = new ProductService($productRepository, $categoryRepository, $datDataService);
    }

    public function testGetProductById()
    {
        $result = $this->service->getProductById(1);
        $this->assertInstanceOf(Product::class, $result);
        $this->assertIsInt($result->id);
    }

    public function testBeforeCreateSave()
    {
        $requestData = [
            'name' => 'create name',
            'inn' => $this->validInn,
            'barcode' => $this->validBarcode,
            'description' => 'create description',
            'categories' => [],
        ];
        $formRequest = new CreateProductFormRequest(
            $requestData,
            Validation::createValidator()
        );
        $this->assertInstanceOf(
            ErrorCollection::class,
            $this->service->beforeSave($formRequest)
        );
        $this->assertEquals(0, $this->service->beforeSave($formRequest)->count());

        $this->assertInstanceOf(CreateProduct::class, $formRequest->getResult());
        $this->assertEquals($requestData['inn'], $formRequest->getResult()->inn);
        $this->assertEquals($requestData['name'], $formRequest->getResult()->name);
        $this->assertEquals($requestData['barcode'], $formRequest->getResult()->barcode);
        $this->assertEquals($requestData['description'], $formRequest->getResult()->description);
        $this->assertEquals($requestData['categories'], $formRequest->getResult()->categories);

        $requestData['inn'] = $this->invalidInn;
        $formRequest = new CreateProductFormRequest($requestData, Validation::createValidator());
        $this->assertEquals(1, $this->service->beforeSave($formRequest)->count());
        $requestData['barcode'] = $this->invalidBarcode;
        $formRequest = new CreateProductFormRequest($requestData, Validation::createValidator());
        $this->assertEquals(2, $this->service->beforeSave($formRequest)->count());
    }

    public function testBeforeUpdateSave()
    {
        $requestData = [
            'id' => 7,
            'name' => 'create name',
            'inn' => $this->validInn,
            'barcode' => $this->validBarcode,
            'description' => 'create description',
        ];
        $formRequest = new UpdateProductFormRequest(
            $requestData,
            Validation::createValidator()
        );
        $this->assertInstanceOf(
            ErrorCollection::class,
            $this->service->beforeSave($formRequest)
        );
        $this->assertEquals(0, $this->service->beforeSave($formRequest)->count());

        $this->assertInstanceOf(UpdateProduct::class, $formRequest->getResult());
        $this->assertEquals($requestData['id'], $formRequest->getResult()->id);
        $this->assertEquals($requestData['inn'], $formRequest->getResult()->inn);
        $this->assertEquals($requestData['name'], $formRequest->getResult()->name);
        $this->assertEquals($requestData['barcode'], $formRequest->getResult()->barcode);
        $this->assertEquals($requestData['description'], $formRequest->getResult()->description);

        $requestData['inn'] = $this->invalidInn;
        $formRequest = new UpdateProductFormRequest($requestData, Validation::createValidator());
        $this->assertEquals(1, $this->service->beforeSave($formRequest)->count());
        $requestData['barcode'] = $this->invalidBarcode;
        $formRequest = new UpdateProductFormRequest($requestData, Validation::createValidator());
        $this->assertEquals(2, $this->service->beforeSave($formRequest)->count());
    }

    /**
     * @throws ProductNotUniqueException
     */
    public function testCreate()
    {
        $dto = new CreateProduct(
            name: 'create name',
            inn: $this->validInn,
            barcode: $this->validBarcode,
            description: 'create description'
        );

        $this->assertInstanceOf(Product::class, $this->service->create($dto));
    }

    public function testFindProductList()
    {
        $this->assertInstanceOf(ProductCollection::class, $this->service->findProductList());
    }

    public function testDelete()
    {
        $this->assertInstanceOf(DeletedProduct::class, $this->service->delete(1));
    }

    public function testUpdate()
    {
        $dto = new UpdateProduct(
            id: 8,
            name: 'update name',
            inn: $this->validInn,
            barcode: $this->validBarcode,
            description: 'update description'
        );
        $this->assertInstanceOf(Product::class, $this->service->update($dto));
    }
}
