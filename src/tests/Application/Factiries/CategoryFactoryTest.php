<?php

namespace tests\Application\Factories;

use App\Application\Factories\CategoryFactory;
use App\Domain\Collections\CategoryCollection;
use PHPUnit\Framework\TestCase;

class CategoryFactoryTest extends TestCase
{
    public function testCollectionFromRequest(): void
    {
        $collection = (new CategoryFactory())->collectionFromRequest([
            ['id' => 1, 'name' => '1 name'],
            ['id' => 2, 'name' => '2 name'],
        ]);

        $this->assertInstanceOf(CategoryCollection::class, $collection);
    }
}
