<?php

namespace tests\Application\Factories;

use App\Application\Factories\ProductFactory;
use App\Domain\Models\Product;
use PHPUnit\Framework\TestCase;

class ProductFactoryTest extends TestCase
{
    public function testFromRequest()
    {
        $product = (new ProductFactory())->fromRequest([
            'id' => 11,
            'name' => 'from request name',
            'inn' => 12384756181,
            'barcode' => 1837491746319,
            'description' => 'from request description',
        ]);

        $this->assertInstanceOf(Product::class, $product);
    }
}
