<?php

namespace tests\Http\Controllers;

use App\Application\Contracts\DaDataClientInterface;
use App\Application\Services\DaDataService;
use App\Application\Services\ProductService;
use App\Domain\Collections\CategoryCollection;
use App\Domain\Collections\ProductCollection;
use App\Domain\Contracts\Infrastructure\CategoryRepositoryInterface;
use App\Domain\Contracts\Infrastructure\ProductRepositoryInterface;
use App\Domain\DTO\DeletedProduct;
use App\Domain\Models\Category;
use App\Domain\Models\Product;
use App\Http\Controllers\Api\ProductsController;
use App\Http\Requests\CreateProductFormRequest;
use App\Http\Requests\UpdateProductFormRequest;
use Core\Contracts\ResponseInterface;
use PHPUnit\Framework\TestCase;
use Core\Response;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use function PHPUnit\Framework\assertEquals;

class ProductsControllerTest extends TestCase
{
    protected $controller;

    protected $response;
    protected $validInn;
    protected $invalidInn;
    protected $validBarcode;
    protected $invalidBarcode;

    public function setUp(): void
    {
        $this->validInn = 7251621782;
        $this->invalidInn = 72516217;
        $this->validBarcode = 9780201379624;
        $this->invalidBarcode = 9780201379627;

        $defaultProduct = new Product(
            id: 1,
            name: 'default name',
            inn: 1111111111,
            barcode: 1222222222223,
            description: 'default description'
        );
        $defaultCollection = new ProductCollection([
            $defaultProduct,
            new Product(
                id: 2,
                name: 'default name',
                inn: 2222222222,
                barcode: 13333333333334,
                description: 'default description'
            ),
        ]);
        $categoryCollection = new CategoryCollection([
            new Category(id: 1, name: 'first'),
            new Category(id: 2, name: 'second'),
            new Category(id: 3, name: 'third'),
            new Category(id: 4, name: 'forthG'),
        ]);
        $productRepository = $this->createStub(ProductRepositoryInterface::class);
        $productRepository->method('getById')
            ->willReturn($defaultProduct);
        $productRepository->method('create')
            ->willReturn($defaultProduct);
        $productRepository->method('isUnique')
            ->willReturn(true);
        $productRepository->method('findProductsAll')
            ->willReturn($defaultCollection);
        $productRepository->method('findBySearchQuery')
            ->willReturn($defaultCollection);
        $productRepository->method('update')
            ->willReturn($defaultProduct);

        $categoryRepository = $this->createStub(CategoryRepositoryInterface::class);
        $categoryRepository->method('findListById')
            ->willReturn($categoryCollection);
        $daDataClient = $this->createStub(DaDataClientInterface::class);
        $daDataClient->method('findByInn')
            ->willReturn([1, 2, 4]);
        $datDataService = new DaDataService($daDataClient);

        $service = new ProductService($productRepository, $categoryRepository, $datDataService);

        $this->controller = new ProductsController($service);
        $this->response = new Response();
    }

    public function testIndex()
    {
        $result = $this->controller->index($this->response);
        $this->assertInstanceOf(Response::class, $result);
        $this->assertInstanceOf(ProductCollection::class, $result->getBody());
        $this->assertInstanceOf(\JsonSerializable::class, $result->getBody());
        $this->assertEquals(200, $result->getStatus());
    }

    public function testCreateValid()
    {
        $requestData = [
            'name' => 'create name',
            'inn' => $this->validInn,
            'barcode' => $this->validBarcode,
            'description' => 'create description',
            'categories' => [],
        ];
        $formRequest = new CreateProductFormRequest(
            $requestData,
            Validation::createValidator()
        );

        $result = $this->controller->create($formRequest, new Response());
        $this->assertInstanceOf(ResponseInterface::class, $result);
        $this->assertIsArray($result->getBody());
        $this->assertArrayHasKey('id', $result->getBody());
        $this->assertArrayHasKey('name', $result->getBody());
        $this->assertArrayHasKey('inn', $result->getBody());
        $this->assertArrayHasKey('barcode', $result->getBody());
        $this->assertArrayHasKey('description', $result->getBody());
        $this->assertEquals(201, $result->getStatus());
    }

    public function testCreateInvalidInn()
    {
        $requestData = [
            'name' => 'create name',
            'inn' => $this->invalidInn,
            'barcode' => $this->validBarcode,
            'description' => 'create description',
            'categories' => [],
        ];
        $formRequest = new CreateProductFormRequest(
            $requestData,
            Validation::createValidator()
        );

        $result = $this->controller->create($formRequest, new Response());
        $this->assertEquals(400, $result->getStatus());
        $body = $result->getBody();
        $this->assertNull($body['data']);
        $this->assertEquals(400, $body['status']);
        $this->assertGreaterThan(0, count($body['errors']));
    }

    public function testCreateInvalidBarcode()
    {
        $requestData = [
            'name' => 'create name',
            'inn' => $this->validInn,
            'barcode' => $this->invalidBarcode,
            'description' => 'create description',
            'categories' => [],
        ];
        $formRequest = new CreateProductFormRequest(
            $requestData,
            Validation::createValidator()
        );

        $result = $this->controller->create($formRequest, new Response());
        $this->assertEquals(400, $result->getStatus());
        $body = $result->getBody();
        $this->assertNull($body['data']);
        $this->assertEquals(400, $body['status']);
        $this->assertGreaterThan(0, count($body['errors']));
    }

    public function testUpdateValid()
    {
        $requestData = [
            'id' => 11,
            'name' => 'create name',
            'inn' => $this->validInn,
            'barcode' => $this->validBarcode,
            'description' => 'create description',
        ];
        $formRequest = new UpdateProductFormRequest(
            $requestData,
            Validation::createValidator()
        );

        $result = $this->controller->update($formRequest, new Response());
        $this->assertInstanceOf(ResponseInterface::class, $result);
        $this->assertIsArray($result->getBody());
        $this->assertArrayHasKey('id', $result->getBody());
        $this->assertArrayHasKey('name', $result->getBody());
        $this->assertArrayHasKey('inn', $result->getBody());
        $this->assertArrayHasKey('barcode', $result->getBody());
        $this->assertArrayHasKey('description', $result->getBody());
        $this->assertEquals(200, $result->getStatus());
    }

    public function testUpdateInvalidInn()
    {
        $requestData = [
            'id' => 11,
            'name' => 'create name',
            'inn' => $this->invalidInn,
            'barcode' => $this->validBarcode,
            'description' => 'create description',
        ];
        $formRequest = new UpdateProductFormRequest(
            $requestData,
            Validation::createValidator()
        );

        $result = $this->controller->update($formRequest, new Response());
        $this->assertEquals(400, $result->getStatus());
        $body = $result->getBody();
        $this->assertNull($body['data']);
        $this->assertEquals(400, $body['status']);
        $this->assertGreaterThan(0, count($body['errors']));
    }

    public function testUpdateInvalidBarcode()
    {
        $requestData = [
            'name' => 'create name',
            'inn' => $this->validInn,
            'barcode' => $this->invalidBarcode,
            'description' => 'create description',
            'categories' => [],
        ];
        $formRequest = new UpdateProductFormRequest(
            $requestData,
            Validation::createValidator()
        );

        $result = $this->controller->update($formRequest, new Response());
        $this->assertEquals(400, $result->getStatus());
        $body = $result->getBody();
        $this->assertNull($body['data']);
        $this->assertEquals(400, $body['status']);
        $this->assertGreaterThan(0, count($body['errors']));
    }

    public function testShow()
    {
        $result = $this->controller->show(11, new Response());
        $body = $result->getBody();

        $this->assertInstanceOf(ResponseInterface::class, $result);
        $this->assertInstanceOf(Product::class, $body['data']);
        $this->assertEquals(200, $result->getStatus());
        $this->assertEquals(200, $body['status']);
    }

    public function testUpdate()
    {
        $result = $this->controller->delete(11, new Response());
        $body = $result->getBody();

        $this->assertInstanceOf(ResponseInterface::class, $result);
        $this->assertInstanceOf(DeletedProduct::class, $body);
        $this->assertEquals(200, $result->getStatus());
    }
}
