<?php

use App\Http\Controllers\Api\IndexController;
use App\Http\Controllers\Api\ProductsController;
use Core\Router\Methods;
use Core\Router\MethodsCollection;
use Core\Router\Route;

return [
    new Route('product_update', '/product/{id}/update', [ProductsController::class, 'update'], (new MethodsCollection())->add(Methods::PUT)),
    new Route('home', '/', [IndexController::class, 'index']),

    // Products
    new Route('product_index', '/product', [ProductsController::class, 'index']),
    new Route('product_create', '/product/create', [ProductsController::class, 'create'], (new MethodsCollection())->add(Methods::POST)),
    new Route('product_show', '/product/{id}/show', [ProductsController::class, 'show'], (new MethodsCollection())->add(Methods::GET)),
    new Route('product_delete', '/product/{id}/delete', [ProductsController::class, 'delete'], (new MethodsCollection())->add(Methods::DELETE)),
];

